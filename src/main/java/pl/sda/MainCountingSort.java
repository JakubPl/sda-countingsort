package pl.sda;

import java.util.Arrays;

public class MainCountingSort {
    public static void main(String[] args) {
        final int[] ints = {6, 3, 7, 1};
        countingSort(ints);
        System.out.println(Arrays.toString(ints));
    }

    private static void countingSort(int[] ints) {
        final int[] histogram = new int[8];
        for (int i = 0; i < ints.length; i++) {
            histogram[ints[i]]++;
        }

        int currentOutPosition = 0;
        for (int i = 0; i < histogram.length; i++) {
            while (histogram[i] > 0) {
                ints[currentOutPosition] = i;
                currentOutPosition++;
                histogram[i]--;
            }
        }
    }
}
